//
//  TestWebViewApp.swift
//  TestWebView
//
//  Created by Jordi Montoya ortega on 30/5/23.
//

import SwiftUI

@main
struct TestWebViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
